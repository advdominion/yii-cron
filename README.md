Агент для работы с кроном
=========================
Функционал для работы с кроном

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist dominion/yii2-cron "*"
```

or add

```
"dominion/yii2-cron": "*"
```

to the require section of your `composer.json` file.


Usage
-----

Once the extension is installed, simply use it in your code by  :

```php
<?= \dominion\cron\AutoloadExample::widget(); ?>```