<?php

namespace dominion\cron\models\search;

use \Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use dominion\cron\models\MoleTask;

/**
 * MoleTaskSearch represents the model behind the search form of `app\models\Articles`.
 */
class MoleTaskSearch extends MoleTask
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'parentId', 'isReady', 'completed', 'priority', 'childsTotalCount', 'childsCompleted', 'period'], 'integer'],
            [['dateAdd', 'dateStart', 'dateEnd'], 'safe'],
            [['params'], 'string'],
            [['module', 'controller', 'type', 'name', 'status'], 'string', 'max' => 255],
            [['project'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MoleTask::find();
        // add conditions that should always apply here

        $this->load($params);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC
                ]
            ]
        ]);

        if (!$this->validate())
        {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }



        $query->andFilterWhere([
            'id' => $this->id,
            'parentId' => $this->parentId,
            'isReady' => $this->isReady,
            'completed' => $this->completed,
            'priority' => $this->priority,
            'childsTotalCount' => $this->childsTotalCount,
            'childsCompleted' => $this->childsCompleted,
        ]);

        $query->andFilterWhere(['like', 'params', $this->params])
            ->andFilterWhere(['like', 'controller', $this->controller])
            ->andFilterWhere(['like', 'module', $this->module])
            ->andFilterWhere(['like', 'type', $this->type])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'project', $this->project]);

        return $dataProvider;
    }

}
