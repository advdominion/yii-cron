<?php

namespace dominion\cron\models;

use Yii;

/**
 * This is the model class for table "mole_task".
 *
 * @property int $id
 * @property int $parentId
 * @property string $dateAdd
 * @property string $dateStart
 * @property string $dateEnd
 * @property string $module
 * @property string $controller
 * @property string $type
 * @property string $name
 * @property string $params
 * @property int $isReady
 * @property int $completed
 * @property int $priority
 * @property int $childsTotalCount
 * @property int $childsCompleted
 * @property string $status
 * @property string $project
 * @property int $period
 */
class MoleTask extends \yii\db\ActiveRecord
{

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mole_task';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            //[['parentId', 'isReady', 'childsTotalCount', 'childsCompleted', 'status'], 'required'],
            [['parentId', 'isReady', 'completed', 'priority', 'childsTotalCount', 'childsCompleted', 'period'], 'integer'],
            [['dateAdd', 'dateStart', 'dateEnd'], 'safe'],
            [['params'], 'string'],
            [['module', 'controller', 'type', 'name', 'status'], 'string', 'max' => 255],
            [['project'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('mole', 'ID'),
            'parentId' => Yii::t('mole', 'Parent ID'),
            'dateAdd' => Yii::t('mole', 'Date Add'),
            'dateStart' => Yii::t('mole', 'Date Start'),
            'dateEnd' => Yii::t('mole', 'Date End'),
            'module' => Yii::t('mole', 'Module'),
            'controller' => Yii::t('mole', 'Controller'),
            'type' => Yii::t('mole', 'Type'),
            'name' => Yii::t('mole', 'Name'),
            'params' => Yii::t('mole', 'Params'),
            'isReady' => Yii::t('mole', 'Is Ready'),
            'completed' => Yii::t('mole', 'Completed'),
            'priority' => Yii::t('mole', 'Priority'),
            'childsTotalCount' => Yii::t('mole', 'Childs Total Count'),
            'childsCompleted' => Yii::t('mole', 'Childs Completed'),
            'status' => Yii::t('mole', 'Status'),
            'project' => Yii::t('mole', 'Project'),
            'period' => Yii::t('mole', 'Period'),
        ];
    }

    /**
     * Выбираем все строки с project
     */
    public function getAllTask($project = false)
    {
        return self::find()
                ->andWhere([
                    'project' => $project ?: Yii::$app->getModule('cron')->project,
                    'completed' => 0,
                ])
                ->andWhere(['IS', 'dateStart', NULL])
                ->andWhere(['IS', 'dateEnd', NULL])
                ->andWhere(['<=', 'dateAdd', date('Y-m-d H:i:s')])
                ->orderBy('priority DESC')
                ->all();
    }

    public function setCompleted()
    {
        $this->dateEnd = date('Y-m-d H:i:s');
        $this->completed = 1;
        if ($this->save() && $this->period > 0)
        {
            $model = new MoleTask;
            $model->attributes = $this->attributes;
            $model->isReady = 0;
            $model->completed = 0;
            $model->dateStart = null;
            $model->dateEnd = null;
            $model->dateAdd = date('Y-m-d H:i:s', (strtotime($this->dateAdd . " +$this->period seconds")));
            $model->save();
        }
    }

    /**
     * Добавление агента (обертка)
     * @param string $controller
     * @param string $name
     * @param array $params
     * @param int $priority
     * @param int $period
     * @param date $dateAdd
     * @param string $project
     * @return boolean
     */
    public static function add($controller, $name, $params = array(), $priority = 0, $period = 0, $dateAdd = false, $project = false)
    {
        $module = '';
        if(stripos($controller, '.') !== false)
        {
            $arController = explode('.', $controller);
            $module = $arController[0];
            $controller = $arController[1];
        }

        $model = new MoleTask();
        $model->controller = $controller;
        $model->name = $name;
        $model->params = serialize($params);
        $model->priority = $priority;
        $model->period = $period;
        $model->dateAdd = $dateAdd ?: date('Y-m-d H:i:s');
        $model->project = $project ?: Yii::$app->getModule('cron')->project;
        $model->module = $module;
        $model->type = ((empty($model->module) ? $model->project : $model->module) . '.' . $model->controller); // для совместимости
        $model->isReady = 1;
        return $model->save();
    }

}
