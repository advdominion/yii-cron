<?php

namespace dominion\cron;

use yii\base\BootstrapInterface;
use yii\i18n\PhpMessageSource;
use dominion\cron\models\MoleTask;

/**
 * cron module definition class
 */
class Module extends \yii\base\Module implements BootstrapInterface
{
    public $admins = [];
    public $project = 'yii';

    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'dominion\cron\controllers';

    public function bootstrap($app)
    {
        if ($app instanceof \yii\web\Application)
        {
            $app->getUrlManager()->addRules([
                ['class' => 'yii\web\UrlRule', 'pattern' => $this->id, 'route' => $this->id . '/default/index'],
                ['class' => 'yii\web\UrlRule', 'pattern' => $this->id . '/<id:\w+>', 'route' => $this->id . '/default/view'],
                ['class' => 'yii\web\UrlRule', 'pattern' => $this->id . '/<controller:[\w\-]+>/<action:[\w\-]+>', 'route' => $this->id . '/<controller>/<action>'],
                ], false);
        }
        elseif ($app instanceof \yii\console\Application)
        {
            $app->controllerMap[$this->id] = [
                'class' => 'dominion\cron\console\MoleController',
              //  'module' => $this,
            ];
        }

        if (!isset($app->get('i18n')->translations['mole*']))
        {
            $app->get('i18n')->translations['mole*'] = [
                'class' => PhpMessageSource::className(),
                'basePath' => __DIR__ . '/messages',
                'sourceLanguage' => 'en-US'
            ];
        }
    }
    /**
     * Добавление агента (обертка)
     * @param string $controller
     * @param string $name
     * @param array $params
     * @param int $priority
     * @param int $period
     * @param date $dateAdd
     * @param string $project
     * @return boolean
     */
    public function add($controller, $name, $params = array(), $priority = 0, $period = 0, $dateAdd = false, $project = false)
    {
        return MoleTask::add($controller, $name, $params, $priority, $period, $dateAdd, $project);
    }

    /**
     * Выбираем все строки с project
     */
    public function getAllTask($project = false)
    {
        $model = new MoleTask();
        return $model->getAllTask($project);
    }


}
