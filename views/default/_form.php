<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MailType */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="mole-form">
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'project'); ?>

    <?= $form->field($model, 'name'); ?>

    <?= $form->field($model, 'dateAdd'); ?>
    <?= $form->field($model, 'dateStart'); ?>
    <?= $form->field($model, 'dateEnd'); ?>

    <?= $form->field($model, 'module'); ?>

    <?= $form->field($model, 'controller'); ?>
    <?= $form->field($model, 'params'); ?>
    <?= $form->field($model, 'period'); ?>

    <div class="form-group">

        <?= Html::submitButton(Yii::t('mole', 'Save'), ['class' => 'btn btn-success']) ?>

    </div>

    <?php ActiveForm::end(); ?>
</div>

