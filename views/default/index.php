<?php
/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\Pjax;

/**
 * @var \yii\web\View $this
 * @var \yii\data\ActiveDataProvider $dataProvider
 * @var \dektrium\user\models\UserSearch $searchModel
 */
$this->title = Yii::t('mole', 'Task List');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="tabs-container">
                <div class="tab-content">
                    <div class="tab-pane active">
                        <div class="panel-body">

                            <p>
                                <?= Html::a(Yii::t('mole', 'Create Task'), ['create'], ['class' => 'btn btn-success']) ?>
                            </p>

                            <?=
                            GridView::widget([
                                'dataProvider' => $dataProvider,
                                'filterModel' => $searchModel,
                                'layout' => "{items}\n{pager}",
                                'columns' => [
                                    'id',
                                    'project',
                                    'name',
                                    // 'parentId',
                                    'dateAdd:datetime',
                                    'dateStart:datetime',
                                    'dateEnd:datetime',
                                    //'module',
                                    'controller',
                                    // 'type',
                                    //'params',
                                    [
                                        'attribute' => 'isReady',
                                        'value' => function($model)
                                        {
                                            return $model->isReady ? Yii::t('mole', 'Yes') : Yii::t('mole', 'No');
                                        },
                                    //   'filter' => [0 => Yii::t('mole', 'No'), 1 => Yii::t('mole', 'Yes')]
                                    ],
                                    [
                                        'attribute' => 'completed',
                                        'value' => function($model)
                                        {
                                            return $model->completed ? Yii::t('mole', 'Yes') : Yii::t('mole', 'No');
                                        },
                                    //    'filter' => [0 => Yii::t('mole', 'No'), 1 => Yii::t('mole', 'Yes')]
                                    ],
                                    'priority',
                                    [
                                        'attribute' => 'period',
                                        'value' => function($model)
                                        {
                                            return $model->period > 0 ? $model->period   : Yii::t('mole', 'No');
                                        },
                                    ],
                                    //'childsTotalCount',
                                    //'childsCompleted',
                                    // 'status',
                                    [
                                        'class' => 'yii\grid\ActionColumn',
                                    ],
                                ],
                            ]);
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
