<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;
use app\components\Helper;

/* @var $this yii\web\View */
/* @var $model app\models\MailType */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('mole', 'Task List'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="tabs-container">
                <div class="tab-content">
                    <div class="tab-pane active">
                        <div class="panel-body">

                            <p>
                                <?= Html::a(Yii::t('mole', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                                <?=
                                Html::a(Yii::t('mole', 'Delete'), ['delete', 'id' => $model->id], [
                                    'class' => 'btn btn-danger',
                                    'data' => [
                                        'confirm' => Yii::t('mole', 'Are you sure you want to delete this item?'),
                                        'method' => 'post',
                                    ],
                                ])
                                ?>
                            </p>

                            <?=
                            DetailView::widget([
                                'model' => $model,
                                'attributes' => [
                                    'id',
                                    'project',
                                    'name',
                                    'parentId',
                                    'dateAdd:datetime',
                                    'dateStart:datetime',
                                    'dateEnd:datetime',
                                    'module',
                                    'controller',
                                    'type',
                                    [
                                        'attribute' => 'params',
                                        'value' => function($model)
                                        {
                                            return print_r(json_decode($model->params, true), true);
                                        },
                                    ],
                                    [
                                        'attribute' => 'isReady',
                                        'value' => function($model)
                                        {
                                            return $model->isReady ? Yii::t('mole', 'Yes') : Yii::t('mole', 'No');
                                        },
                                    ],
                                    [
                                        'attribute' => 'completed',
                                        'value' => function($model)
                                        {
                                            return $model->completed ? Yii::t('mole', 'Yes') : Yii::t('mole', 'No');
                                        },
                                    ],
                                    'priority',
                                    'status',
                                    [
                                        'attribute' => 'period',
                                        'value' => function($model)
                                        {
                                            return $model->period > 0 ? $model->period   : Yii::t('mole', 'No');
                                        },
                                    ],
                                ],
                            ])
                            ?>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

