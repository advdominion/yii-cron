<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MailType */

$this->title = Yii::t('mole', 'Create Task');
$this->params['breadcrumbs'][] = ['label' => Yii::t('mole', 'Task List'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="wrapper wrapper-content">
	<div class="row">
		<div class="col-lg-12">
			<div class="tabs-container">
				<div class="tab-content">
					<div class="tab-pane active">
						<div class="panel-body">
							<?=
							$this->render('_form', [
								'model' => $model,
							])
							?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>